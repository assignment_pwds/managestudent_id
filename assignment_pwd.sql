-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2024 at 04:44 PM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assignment_pwd`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblacademicyear`
--

CREATE TABLE `tblacademicyear` (
  `AcademicYearID` int(11) NOT NULL,
  `AcademicYear` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblbatch`
--

CREATE TABLE `tblbatch` (
  `BatchID` int(11) NOT NULL,
  `BatchKH` varchar(20) NOT NULL,
  `BatchEN` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblcampus`
--

CREATE TABLE `tblcampus` (
  `CampusID` int(11) NOT NULL,
  `CampusKH` varchar(20) NOT NULL,
  `CampusEN` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblcountry`
--

CREATE TABLE `tblcountry` (
  `CountryID` int(11) NOT NULL,
  `CountryKH` varchar(20) NOT NULL,
  `CountryEN` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbldegree`
--

CREATE TABLE `tbldegree` (
  `DegreeID` int(11) NOT NULL,
  `DegreeNameKH` varchar(20) NOT NULL,
  `DegreeNameEN` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbleducationalbackground`
--

CREATE TABLE `tbleducationalbackground` (
  `EducationalBackgroundID` int(11) NOT NULL,
  `SchoolTypeID` int(11) NOT NULL,
  `NameSchool` varchar(20) NOT NULL,
  `AcademicYear` int(11) NOT NULL,
  `Province` varchar(20) NOT NULL,
  `StudentID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblfaculty`
--

CREATE TABLE `tblfaculty` (
  `FacultyID` int(11) NOT NULL,
  `FacultyKH` varchar(20) NOT NULL,
  `FacultyEN` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblfamilybackground`
--

CREATE TABLE `tblfamilybackground` (
  `FamilyBackgroundID` int(11) NOT NULL,
  `FatherName` varchar(20) NOT NULL,
  `FatherAge` int(2) NOT NULL,
  `FatherNationalityID` int(11) NOT NULL,
  `FatherCountryID` int(11) NOT NULL,
  `FatherOccupationID` int(11) NOT NULL,
  `MotherName` varchar(20) NOT NULL,
  `MotherAge` int(11) NOT NULL,
  `MotherNationalityID` int(11) NOT NULL,
  `MotherCountryID` int(11) NOT NULL,
  `MotherOccupationID` int(11) NOT NULL,
  `FamilyCurrentAddress` int(11) NOT NULL,
  `SpouseName` varchar(20) NOT NULL,
  `SpouseAge` int(11) NOT NULL,
  `GuardianPhoneNumber` int(10) NOT NULL,
  `StudentID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblmajor`
--

CREATE TABLE `tblmajor` (
  `MajorID` int(11) NOT NULL,
  `MajorKH` varchar(20) NOT NULL,
  `MjaorEN` varchar(20) NOT NULL,
  `FacultyID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblnationality`
--

CREATE TABLE `tblnationality` (
  `NationalityID` int(11) NOT NULL,
  `NationalityKH` varchar(20) NOT NULL,
  `NationalityEN` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblprogram`
--

CREATE TABLE `tblprogram` (
  `ProgramID` int(11) NOT NULL,
  `YearID` int(11) NOT NULL,
  `SemesterID` int(11) NOT NULL,
  `ShiftID` int(11) NOT NULL,
  `DegreeID` int(11) NOT NULL,
  `AcademicYearID` int(11) NOT NULL,
  `MajorID` int(11) NOT NULL,
  `BatchID` int(11) NOT NULL,
  `CampusID` int(11) NOT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date NOT NULL,
  `DateIsue` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblschooltype`
--

CREATE TABLE `tblschooltype` (
  `SchoolTypeID` int(11) NOT NULL,
  `SchoolTypeKH` varchar(20) NOT NULL,
  `SchoolTypeEN` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblsemester`
--

CREATE TABLE `tblsemester` (
  `SemesterID` int(11) NOT NULL,
  `SemesterKH` varchar(20) NOT NULL,
  `SemesterEN` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblsex`
--

CREATE TABLE `tblsex` (
  `SexID` int(11) NOT NULL,
  `SexKH` varchar(20) NOT NULL,
  `SexEN` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblshift`
--

CREATE TABLE `tblshift` (
  `ShiftID` int(11) NOT NULL,
  `ShiftKH` varchar(20) NOT NULL,
  `ShiftEN` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblstudentinfo`
--

CREATE TABLE `tblstudentinfo` (
  `StudentID` int(11) NOT NULL,
  `NameInKhmer` varchar(20) NOT NULL,
  `NameInLatin` varchar(20) NOT NULL,
  `FamilyName` varchar(20) NOT NULL,
  `GivenName` varchar(20) NOT NULL,
  `SexID` int(11) NOT NULL,
  `IDPassportNo` int(11) NOT NULL,
  `NationalityID` int(11) NOT NULL,
  `CountryID` int(11) NOT NULL,
  `DOB` int(11) NOT NULL,
  `POB` int(11) NOT NULL,
  `PhoneNumber` int(11) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `CurrentAddress` varchar(20) NOT NULL,
  `CurrentAddressPP` varchar(20) NOT NULL,
  `Photo` varchar(30) NOT NULL,
  `RegisterDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblstudentstatus`
--

CREATE TABLE `tblstudentstatus` (
  `StudentStatusID` int(11) NOT NULL,
  `StudentID` int(11) NOT NULL,
  `ProgramID` int(11) NOT NULL,
  `Assigned` varchar(100) NOT NULL,
  `Note` text NOT NULL,
  `AssignDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblsubject`
--

CREATE TABLE `tblsubject` (
  `SubjectID` int(11) NOT NULL,
  `SubjectKH` int(20) NOT NULL,
  `SubjectEN` int(20) NOT NULL,
  `CreditNumber` int(11) NOT NULL,
  `Hours` time NOT NULL,
  `MajorID` int(11) NOT NULL,
  `YearID` int(11) NOT NULL,
  `SemesterID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblyear`
--

CREATE TABLE `tblyear` (
  `YearID` int(11) NOT NULL,
  `YearKH` varchar(20) NOT NULL,
  `YearEN` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblacademicyear`
--
ALTER TABLE `tblacademicyear`
  ADD PRIMARY KEY (`AcademicYearID`);

--
-- Indexes for table `tblbatch`
--
ALTER TABLE `tblbatch`
  ADD PRIMARY KEY (`BatchID`);

--
-- Indexes for table `tblcampus`
--
ALTER TABLE `tblcampus`
  ADD PRIMARY KEY (`CampusID`);

--
-- Indexes for table `tblcountry`
--
ALTER TABLE `tblcountry`
  ADD PRIMARY KEY (`CountryID`);

--
-- Indexes for table `tbldegree`
--
ALTER TABLE `tbldegree`
  ADD PRIMARY KEY (`DegreeID`);

--
-- Indexes for table `tbleducationalbackground`
--
ALTER TABLE `tbleducationalbackground`
  ADD PRIMARY KEY (`EducationalBackgroundID`);

--
-- Indexes for table `tblfaculty`
--
ALTER TABLE `tblfaculty`
  ADD PRIMARY KEY (`FacultyID`);

--
-- Indexes for table `tblfamilybackground`
--
ALTER TABLE `tblfamilybackground`
  ADD PRIMARY KEY (`FamilyBackgroundID`);

--
-- Indexes for table `tblmajor`
--
ALTER TABLE `tblmajor`
  ADD PRIMARY KEY (`MajorID`);

--
-- Indexes for table `tblnationality`
--
ALTER TABLE `tblnationality`
  ADD PRIMARY KEY (`NationalityID`);

--
-- Indexes for table `tblprogram`
--
ALTER TABLE `tblprogram`
  ADD PRIMARY KEY (`ProgramID`);

--
-- Indexes for table `tblschooltype`
--
ALTER TABLE `tblschooltype`
  ADD PRIMARY KEY (`SchoolTypeID`);

--
-- Indexes for table `tblsemester`
--
ALTER TABLE `tblsemester`
  ADD PRIMARY KEY (`SemesterID`);

--
-- Indexes for table `tblsex`
--
ALTER TABLE `tblsex`
  ADD PRIMARY KEY (`SexID`);

--
-- Indexes for table `tblshift`
--
ALTER TABLE `tblshift`
  ADD PRIMARY KEY (`ShiftID`);

--
-- Indexes for table `tblstudentinfo`
--
ALTER TABLE `tblstudentinfo`
  ADD PRIMARY KEY (`StudentID`);

--
-- Indexes for table `tblstudentstatus`
--
ALTER TABLE `tblstudentstatus`
  ADD PRIMARY KEY (`StudentStatusID`);

--
-- Indexes for table `tblsubject`
--
ALTER TABLE `tblsubject`
  ADD PRIMARY KEY (`SubjectID`);

--
-- Indexes for table `tblyear`
--
ALTER TABLE `tblyear`
  ADD PRIMARY KEY (`YearID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblacademicyear`
--
ALTER TABLE `tblacademicyear`
  MODIFY `AcademicYearID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblbatch`
--
ALTER TABLE `tblbatch`
  MODIFY `BatchID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblcampus`
--
ALTER TABLE `tblcampus`
  MODIFY `CampusID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblcountry`
--
ALTER TABLE `tblcountry`
  MODIFY `CountryID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbldegree`
--
ALTER TABLE `tbldegree`
  MODIFY `DegreeID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbleducationalbackground`
--
ALTER TABLE `tbleducationalbackground`
  MODIFY `EducationalBackgroundID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblfaculty`
--
ALTER TABLE `tblfaculty`
  MODIFY `FacultyID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblfamilybackground`
--
ALTER TABLE `tblfamilybackground`
  MODIFY `FamilyBackgroundID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblmajor`
--
ALTER TABLE `tblmajor`
  MODIFY `MajorID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblnationality`
--
ALTER TABLE `tblnationality`
  MODIFY `NationalityID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblprogram`
--
ALTER TABLE `tblprogram`
  MODIFY `ProgramID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblschooltype`
--
ALTER TABLE `tblschooltype`
  MODIFY `SchoolTypeID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblsemester`
--
ALTER TABLE `tblsemester`
  MODIFY `SemesterID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblsex`
--
ALTER TABLE `tblsex`
  MODIFY `SexID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblshift`
--
ALTER TABLE `tblshift`
  MODIFY `ShiftID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblstudentinfo`
--
ALTER TABLE `tblstudentinfo`
  MODIFY `StudentID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblstudentstatus`
--
ALTER TABLE `tblstudentstatus`
  MODIFY `StudentStatusID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblsubject`
--
ALTER TABLE `tblsubject`
  MODIFY `SubjectID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblyear`
--
ALTER TABLE `tblyear`
  MODIFY `YearID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
